#ifndef LA_TYPEDEF_H
#define LA_TYPEDEF_H 1

#include <stddef.h>

struct la_typedef;

struct la_typedef_array
{
	size_t elem_count;
	const struct la_typedef *elem_type;
};

/* Only allowed inside structures. */
struct la_typedef_padding
{
	size_t bit_count;
};

enum la_scalar_type
{
	LA_SCALAR_TYPE_INT = 0,
	LA_SCALAR_TYPE_UINT,
	LA_SCALAR_TYPE_FLOAT,
	LA_SCALAR_TYPE_INORM,
	LA_SCALAR_TYPE_UNORM,
	LA_SCALAR_TYPE_FNORM
};

struct la_typedef_scalar
{
	enum la_scalar_type type;
	size_t bit_size;
};

struct la_typedef_struct_member
{
	/* If type is not padding name must be a valid \0-terminated string. */
	const char *name;
	const struct la_typedef *type;
};

struct la_typedef_struct
{
	size_t member_count;
	const struct la_typedef_struct_member *members;
	/*
	 * If packed the members are tightly packed together, but can be padded.
	 * Else each member (except padding) as well as the whole structure size
	 * is aligned and padded to match the current platform alignment.
	 */
	unsigned int packed : 1;
};

enum la_typedef_type
{
	LA_TYPEDEF_TYPE_ARRAY = 0,
	LA_TYPEDEF_TYPE_PADDING,
	LA_TYPEDEF_TYPE_SCALAR,
	LA_TYPEDEF_TYPE_STRUCT
};

struct la_typedef
{
	enum la_typedef_type def_type;
	union
	{
		struct la_typedef_array array;
		struct la_typedef_padding padding;
		struct la_typedef_scalar scalar;
		struct la_typedef_struct st;
	} u;
};


int la_typedef_cmp(const struct la_typedef *def0, const struct la_typedef *def1);

#endif /* LA_TYPEDEF_H */
