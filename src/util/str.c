#include <string.h>

#include "str.h"

int la_u_strcmp_null(const char *s1, const char *s2)
{
	if (s1 == s2)
		return 0;

	if (!s1)
		return -1;

	if (!s2)
		return 1;

	return strcmp(s1, s2);
}
