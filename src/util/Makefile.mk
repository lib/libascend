SUBDIRS-y :=
EXTDIRS-y :=

libascend-util := libascend-util$(STATIC_LIB_EXT)

STATICLIBS-y := $(libascend-util)

objs := \
	str.c.o

OBJS-$(libascend-util)-y := $(objs)

CFLAGS-y += -fPIC
