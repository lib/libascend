SUBDIRS-y := util
EXTDIRS-y :=

libascend := libascend$(DYN_LIB_EXT)

BINS-y := $(libascend)
INSTALL_LIBS-y := $(libascend)

libs := \
	util/libascend-util$(STATIC_LIB_EXT)

objs := \
	typedef.c.o

LIBS-$(libascend)-y := $(libs)
OBJS-$(libascend)-y := $(objs)

CFLAGS-y += -fPIC -I$(MKS_PROJDIR)/include/libascend -I$(MKS_PROJDIR)/src

LDFLAGS-$(libascend)-y += $(LDFLAGS_SHARED)
