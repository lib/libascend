#include <string.h>

#include "util/str.h"

#include "typedef.h"

static int la_typedef_struct_member_cmp(const struct la_typedef_struct_member *mb0, const struct la_typedef_struct_member *mb1)
{
	if (la_typedef_cmp(mb0->type, mb1->type) < 0)
		return -1;

	if (mb0->type->def_type == LA_TYPEDEF_TYPE_PADDING
			&& la_u_strcmp_null(mb0->name, mb1->name) != 0)
		return -1;

	if (strcmp(mb0->name, mb1->name) != 0)
		return -1;

	return 0;
}

int la_typedef_cmp(const struct la_typedef *def0, const struct la_typedef *def1)
{
	if (def0 == def1)
		return 0;

	if (def0->def_type != def1->def_type)
		return -1;

	switch (def0->def_type)
	{
		case LA_TYPEDEF_TYPE_SCALAR:
			if (def0->u.scalar.type != def1->u.scalar.type
					|| def0->u.scalar.bit_size != def1->u.scalar.bit_size)
				return -1;
			break;

		case LA_TYPEDEF_TYPE_ARRAY:
			if (def0->u.array.elem_count != def1->u.array.elem_count
					|| la_typedef_cmp(def0->u.array.elem_type, def1->u.array.elem_type) < 0)
				return -1;
			break;

		case LA_TYPEDEF_TYPE_STRUCT:
			if (def0->u.st.member_count != def1->u.st.member_count)
				return -1;

			for (size_t i = 0; i < def0->u.st.member_count; i++)
			{
				if (la_typedef_struct_member_cmp(&def0->u.st.members[i], &def1->u.st.members[i]) < 0)
					return -1;
			}
			break;

		case LA_TYPEDEF_TYPE_PADDING:
			if (def0->u.padding.bit_count != def1->u.padding.bit_count)
				return -1;
			break;

		default:
			return -1;
	}

	return 0;
}
